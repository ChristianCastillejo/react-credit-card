import { combineReducers } from "redux";
import { cardReducer } from "./card_reducer";

const rootReducer = combineReducers({
  card: cardReducer
});

export default rootReducer;
