import React from "react";
import { Icon, Grid, Dimmer, Header } from "semantic-ui-react";
import styles from "../assets/styles";
import moment from "moment";
const CardView = ({ card, handleFieldChange }) => {
  return (
    <div style={styles.background}>
      <Dimmer active={!card.data && !card.error} page inverted>
        <Header style={styles.header} as="h3" icon>
          <Icon loading name="spinner" />
          Loading...
        </Header>
      </Dimmer>
      {card.error ? (
        <Header style={styles.header} as="h3" icon>
          <Icon name="refresh" />
          Upss unable to connect, please try again
        </Header>
      ) : (
        card.data && (
          <div style={styles.container}>
            <div style={styles.details}>
              <div style={styles.card}>
                <p style={styles.name}>{card.data.account.name}</p>
                <p style={styles.iban}>{card.data.account.iban}</p>
                <p style={styles.balance}>
                  Balance:{" "}
                  <span style={styles.balanceAmount}>
                    €{card.data.account.balance}
                  </span>
                </p>
                <span>
                  <Icon size="big" name="cc visa" style={styles.visa} />
                </span>
              </div>
            </div>
            <div style={styles.list}>
              {card.data.debitsAndCredits.map((transaction, index) => (
                <Grid style={styles.items} key={index}>
                  <Grid.Row colums={3} verticalAlign="middle">
                    <Grid.Column width={4}>
                      <p style={styles.date}>
                        {moment(transaction.date).format("MM DD YY h:mm")}
                      </p>
                    </Grid.Column>
                    <Grid.Column width={7}>
                      <p style={styles.fromOrTo}>
                        {transaction.to ? transaction.to : transaction.from}
                      </p>
                      <p>{transaction.description}</p>
                    </Grid.Column>
                    <Grid.Column width={5}>
                      <p
                        style={{
                          ...styles.amount,
                          background: transaction.from
                            ? "linear-gradient(120deg, #4cd964, #adeeb8)"
                            : "linear-gradient(120deg, #ff3b30, #ff9892)"
                        }}
                      >
                        {transaction.from ? "+ €" : "- €"}
                        {transaction.amount
                          ? transaction.amount
                          : transaction.debit}
                      </p>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
              ))}
            </div>
          </div>
        )
      )}
    </div>
  );
};

export default CardView;
