const windowHeight = window.innerHeight;
const windowWidth = window.innerWidth;

export default {
  background: {
    display: "flex",
    justifyContent: "center",
    background: "linear-gradient(120deg, #fafafa, #ffffff)",
    height: windowHeight
  },
  container: {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
    marginTop: windowWidth > 700 && windowHeight * 0.05,
    backgroundColor: "#fff",
    boxShadow: "0px 3px 2px 2px #eeeeee",
    height: windowWidth > 700 ? windowHeight * 0.9 : windowHeight,
    borderRadius: windowWidth > 700 && 10,
    padding: 30,
    width: windowWidth > 700 ? windowWidth * 0.4 : windowWidth
  },
  details: { height: windowHeight * 0.3 },
  card: {
    display: "flex",
    flexDirection: "column",
    justifyContent: "space-around",
    boxShadow: "0px 1px 3px 2px #eeeeee",
    borderRadius: 10,
    background: "linear-gradient(to bottom right, #007aff, #5ac8fa)",
    backgroundColor: "",
    height: windowWidth < 700 ? windowHeight * 0.32 : windowHeight * 0.28,
    width: windowWidth < 700 ? windowHeight * 0.5 : windowHeight * 0.42,
    padding: 20,
    color: "#fff"
  },
  list: {
    marginTop: 30,
    overflow: "auto",
    height: windowHeight * 0.6,
    width: windowWidth < 700 ? "110%" : "80%"
  },
  items: {
    color: "#676767",
    background: "linear-gradient(120deg, #fafafa, #fefefe)",
    fontSize: 12,
    alignItems: "center",
    display: "flex",
    flexDirection: "row",
    margin: 10,
    marginBottom: 15,
    height: 50,
    borderRadius: 5,
    boxShadow: "0px 1px 3px 2px #eeeeee"
  },
  amount: {
    fontSize: 11,
    color: "#fff",
    borderRadius: 7,
    textAlign: "center",
    padding: 5,
    fontWeight: "bold"
  },
  header: { marginTop: 0.4 * windowHeight, color: "#676767" },
  name: { fontWeight: "bold", fontSize: 25, marginBottom: 5, marginTop: 15 },
  iban: { fontSize: 20, marginBottom: 30 },
  balance: { fontSize: windowWidth < 700 ? 20 : 15 },
  balanceAmount: { fontWeight: "bold" },
  visa: {
    float: "right",
    marginTop: windowWidth < 700 ? -50 : -41,
    fontSize: windowWidth < 700 && 40
  },
  date: { fontSize: 11 },
  fromOrTo: { marginBottom: 0, fontWeight: "bold" }
};
