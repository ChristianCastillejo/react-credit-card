import React, { Component } from "react";
import { connect } from "react-redux";
import _ from "lodash";
import { fetchDetails } from "../actions";
import CardView from "./card_view";

class Card extends Component {
  constructor(props) {
    super(props);
    this.state = { card: this.props.card };
    this.props.fetchDetails();
  }

  componentWillReceiveProps(nextProps) {
    if (!_.isEqual(this.props.card, nextProps.card)) {
      this.setState({ card: nextProps.card });
    }
  }

  render() {
    return (
      <CardView
        card={this.state.card}
        handleFieldChange={this.handleFieldChange}
      />
    );
  }
}

const mapStateToProps = ({ card }) => ({
  card
});

const actions = {
  fetchDetails
};

export default connect(mapStateToProps, actions)(Card);
