import React from "react";
import { Provider } from "react-redux";
import { BrowserRouter as Router, Route } from "react-router-dom";
import "semantic-ui-css/semantic.min.css";

import store from "./store";

import Card from "./components/card";

const App = () => {
  return (
    <Provider store={store}>
      <Router>
        <Route exact path="/" component={Card} />
      </Router>
    </Provider>
  );
};

export default App;
