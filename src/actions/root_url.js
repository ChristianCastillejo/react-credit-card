const ROOT_DOMAIN = "192.168.0.73:8080";
const ROOT_URL = `http://${ROOT_DOMAIN}`;

export default ROOT_URL;
