import { axiosInstance } from "./configured_axios";
import { FETCH_DETAILS, FETCH_DETAILS_ERROR } from "./action_types";

export function fetchDetails() {
  return dispatch => {
    axiosInstance
      .get(`/api/getbalance`)
      .then(response => {
        dispatch({ type: FETCH_DETAILS, payload: response });
      })
      .catch(error => dispatch({ type: FETCH_DETAILS_ERROR, payload: error }));
  };
}
