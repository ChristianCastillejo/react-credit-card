import { FETCH_DETAILS, FETCH_DETAILS_ERROR } from "../actions/action_types";

export function cardReducer(state = [], action) {
  switch (action.type) {
    case FETCH_DETAILS: {
      return { data: action.payload.data, error: false };
    }
    case FETCH_DETAILS_ERROR: {
      return { data: {}, error: true };
    }
    default: {
      return state;
    }
  }
}
